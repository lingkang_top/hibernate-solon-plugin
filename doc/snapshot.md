## 如何使用快照版本？

在Maven仓库中配置快照版的公共仓库:

```xml
<repositories>
    <repository>
        <id>nexus</id>
        <name>Nexus</name>
        <layout>default</layout>
        <url>https://s01.oss.sonatype.org/content/repositories/snapshots</url>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
        <releases>
            <enabled>true</enabled>
        </releases>
    </repository>
</repositories>
```

再引入快照版本依赖

```xml
<dependency>
    <groupId>top.lingkang</groupId>
    <artifactId>hibernate-solon-plugin</artifactId>
    <!--  快照版本  -->
    <version>0.0.8-SNAPSHOT</version>
</dependency>
```

有哪些快照版本可以看这里：
[https://s01.oss.sonatype.org/content/repositories/snapshots/top/lingkang/hibernate-solon-plugin/](https://s01.oss.sonatype.org/content/repositories/snapshots/top/lingkang/hibernate-solon-plugin/)
