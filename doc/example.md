## 基础例子

## 1、单表复杂条件查询

```java
    public void testCriteria() {
        Session currentSession = sessionFactory.openSession();
        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<UserEntity> criteria = builder.createQuery(UserEntity.class);
        Root<UserEntity> from = criteria.from(UserEntity.class);
        criteria.select(from);
        List<Predicate> predicates = new ArrayList<>();

        // 假设条件
        if ("id".equals("id"))
            predicates.add(builder.equal(from.get("id"), "admin123"));
        if ("id".equals("id"))
            predicates.add(builder.like(from.get("id"), "%a%"));
        criteria.where(predicates.toArray(new Predicate[0]));
        // 排序
        criteria.orderBy(
            new OrderImpl(from.get("id"),true),
            new OrderImpl(from.get("createTime"),false)
        );// true 为 asc，false为desc

        List<UserEntity> resultList = currentSession.createQuery(criteria).getResultList();
        log.info("testCriteria: {}", resultList);
        }
```

## 2、自定义Bean解析

```java
List<UserConsumeVo> resultList = sessionFactory.openSession().createQuery(
"select o.price as price,o.completionTime as completionTime,o.status as status,o.id as orderId,o.description as description" +
        " from UserEntity u left join OrderUserEntity ou on u.id=ou.userId" +
        " left join OrderEntity o on o.id=ou.orderId where u.openId=?1" +
        " and o.status in ('1','2') order by o.id desc"
).setResultTransformer(new TupleResult(UserConsumeVo.class))// 此处设置自定义解析
.setParameter(1, openId)
.setMaxResults(size).setFirstResult((page - 1) * size)
.list();
```

