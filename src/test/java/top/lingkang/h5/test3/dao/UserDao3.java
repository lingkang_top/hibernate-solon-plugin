package top.lingkang.h5.test3.dao;

import top.lingkang.h5.test3.entity.UserEntity3;
import top.lingkang.hibernate5.dao.Dao;
import top.lingkang.hibernate5.dao.HibernateDao;

/**
 * @author lingkang
 * created by 2023/9/28
 */
@Dao
public interface UserDao3 extends HibernateDao<UserEntity3> {
}
