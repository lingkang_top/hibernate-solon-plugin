package top.lingkang.h5.test3;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.noear.solon.annotation.Import;
import org.noear.solon.annotation.Inject;
import org.noear.solon.test.SolonJUnit4ClassRunner;

import java.util.List;

/**
 * @author lingkang
 * created by 2023/10/19
 * 过滤器测试
 */
@Slf4j
@Import(profiles = "classpath:app.yml")
@RunWith(SolonJUnit4ClassRunner.class)
public class TestFilter {
    @Inject
    private SessionFactory sessionFactory;

    @Test
    public void testFilter() {
        Session session = sessionFactory.getCurrentSession();
        session.enableFilter("userFilter").setParameter("username", "admin");
        List list = session.createQuery(
                "select e from UserEntity3 e"
        ).list();
        log.info("testFilter user list: {}", list);
        List list1 = session.createQuery("select e from OrderEntity3 e").list();
        log.info("testFilter user list1: {}", list1);
    }
}
