package top.lingkang.h5.test3.entity;

import lombok.Data;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import java.util.Date;

/**
 * @author lingkang
 * created by 2023/9/27
 * mysql:
 *
 * <pre>
 * CREATE TABLE `n_user` (
 *   `id` varchar(50) NOT NULL,
 *   `username` varchar(50) DEFAULT NULL,
 *   `password` varchar(64) DEFAULT NULL,
 *   `nickname` varchar(50) DEFAULT NULL,
 *   `sex` varchar(2) DEFAULT NULL,
 *   `create_time` datetime DEFAULT NULL,
 *   `update_time` datetime DEFAULT NULL,
 *   PRIMARY KEY (`id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 * </pre>
 */
@Data
@Entity
@Table(name = "n_user")
@FilterDef(
        name = "userFilter",
        parameters = {@ParamDef(name = "username", type = "string")}
)
@Filters({@Filter(name = "userFilter",condition = "username=:username")})
public class UserEntity3 {
    @Id
    private String id;
    @Column
    private String username;
    @Column
    private String password;
    @Column
    private String nickname;
    @Column
    private String sex;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "update_time")
    private Date updateTime;

    @PrePersist
    public void prePersist() {
        if (createTime == null)
            createTime = new Date();
        if (updateTime == null)
            updateTime = createTime;
        else
            updateTime = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        updateTime = new Date();
    }
}
