package top.lingkang.h5.test.config;

import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AvailableSettings;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;
import top.lingkang.hibernate5.config.HibernateConfiguration;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author lingkang
 * created by 2023/9/27
 */
@Configuration
public class MyConfig {
    @Bean
    public DataSource db1(@Inject("${db1}") HikariDataSource ds) {
        ds.setPoolName("db1");
        ds.setMaximumPoolSize(10);
        return ds;
    }

    @Bean
    public SessionFactory sessionFactory(@Inject DataSource dataSource) {
        Properties properties = new Properties();
        properties.put(AvailableSettings.ISOLATION, 1);
        properties.put(AvailableSettings.DATASOURCE, dataSource);


        /**
         * create：表示启动的时候先drop，再create
         * create-drop: 也表示创建，只不过再系统关闭前执行一下drop
         * update: 这个操作启动的时候会去检查schema是否一致，如果不一致会做scheme更新
         * validate: 启动时验证现有schema与你配置的hibernate是否一致，如果不一致就抛出异常，并不做更新
         */
        properties.put(AvailableSettings.HBM2DDL_AUTO, "validate");

        properties.put(AvailableSettings.SHOW_SQL, "true");
        HibernateConfiguration configuration = new HibernateConfiguration();
        // 扫描实体类所在的包
        configuration.addScanPackage("top.lingkang.h5.entity");
        // 或者一个个加
        // configuration.addAnnotatedClass(UserEntity.class);
        configuration.setProperties(properties);
        // configuration.setInterceptor(new MyInterceptor());
        return configuration.buildSessionFactory();
    }
}
