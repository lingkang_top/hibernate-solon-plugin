package top.lingkang.h5.test.config;

import lombok.extern.slf4j.Slf4j;
import org.noear.solon.annotation.Init;
import org.noear.solon.annotation.Inject;
import top.lingkang.h5.test.dao.UserDao;
import top.lingkang.h5.entity.UserEntity;
import top.lingkang.hibernate5.transaction.Transaction;

import java.util.List;

/**
 * @author lingkang
 * created by 2023/10/16
 * 测试 @Init 注解的aop事务
 */
@Slf4j
// @Component
public class TestInit {
    @Inject
    private UserDao userDao;

    @Transaction
    @Init
    public void init() {
        List<UserEntity> all = userDao.getAll();
        log.info("TestInit aop transaction: {}", all);
        UserEntity entity = all.get(0);
        entity.setPassword(System.currentTimeMillis() + "");
        userDao.saveOrUpdate(entity);
    }
}
