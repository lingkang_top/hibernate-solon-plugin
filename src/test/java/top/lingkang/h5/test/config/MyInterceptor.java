package top.lingkang.h5.test.config;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.EmptyInterceptor;
import org.hibernate.Transaction;

/**
 * @author lingkang
 * created by 2023/11/10
 */
@Slf4j
public class MyInterceptor extends EmptyInterceptor {

    @Override
    public String onPrepareStatement(String sql) {
        log.info("onPrepareStatement");
        return sql;
    }

    @Override
    public void afterTransactionCompletion(Transaction tx) {
        super.afterTransactionCompletion(tx);
    }

    @Override
    public void beforeTransactionCompletion(Transaction tx) {
        super.beforeTransactionCompletion(tx);
    }
}
