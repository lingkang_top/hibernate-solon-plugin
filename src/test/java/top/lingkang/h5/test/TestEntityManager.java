package top.lingkang.h5.test;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.junit.Test;
import top.lingkang.h5.entity.UserEntity;
import top.lingkang.hibernate5.transaction.Transaction;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * @author lingkang
 * created by 2023/10/9
 * JPA的 EntityManager 测试
 */
@Slf4j
public class TestEntityManager extends BaseTest {
    @Transaction
    @Test
    public void testEntityManager() {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.createQuery(
                        "update UserEntity set password=?1 where id=?2"
                ).setParameter(1, "currentSession-" + System.currentTimeMillis())
                .setParameter(2, "admin").executeUpdate();
        EntityManager entityManager = sessionFactory.createEntityManager();
        UserEntity entity = entityManager.find(UserEntity.class, "admin");
        System.out.println(entity);
    }

    @Test
    public void testSelect() {
        EntityManager entityManager = sessionFactory.createEntityManager();
        UserEntity entity = entityManager.find(UserEntity.class, "admin");
        System.out.println(entity);
    }

    @Test
    public void testSelectMap() {
        EntityManager entityManager = sessionFactory.createEntityManager(new HashMap());
        UserEntity entity = entityManager.find(UserEntity.class, "admin");
        System.out.println(entity);
    }

    @Test
    public void testSelectMapSynchronizationType() {
        EntityManager entityManager = sessionFactory.createEntityManager();
        UserEntity entity = entityManager.find(UserEntity.class, "admin");
        System.out.println(entity);
    }

    @Transaction
    @Test
    public void testAdd() {
        EntityManager entityManager = sessionFactory.createEntityManager();
        UserEntity entity = new UserEntity();
        entity.setId(System.currentTimeMillis() + "");
        entity.setUsername("TestEntityManager");
        entity.setPassword(UUID.randomUUID().toString());
        entityManager.persist(entity);
        log.info("testAdd: {}", entityManager.find(UserEntity.class, entity.getId()));
        entityManager.remove(entity);
    }

    @Transaction
    @Test
    public void testUpdate() {
        EntityManager entityManager = sessionFactory.createEntityManager();
        List<UserEntity> list = entityManager.createQuery("select e from UserEntity e").getResultList();
        if (list.isEmpty())
            return;
        UserEntity entity = list.get(0);
        entity.setSex("男".equals(entity.getSex()) ? "女" : "男");
        entityManager.persist(entity);
        log.info("testUpdate: {}", entity);
    }

    @Test
    public void testCriteria() {
        EntityManager em = sessionFactory.createEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<UserEntity> criteria = builder.createQuery(UserEntity.class);
        Root<UserEntity> root = criteria.from(UserEntity.class);
        criteria.select(root);
        criteria.where(builder.equal(root.get("id"), "admin"));
        List<UserEntity> resultList = em.createQuery(criteria).getResultList();
        log.info("testCriteria: {}", resultList);
    }

    @Test
    public void testNotTran() {
        EntityManager em = sessionFactory.createEntityManager();
        List list = em.createQuery("select e from UserEntity e").getResultList();
        log.info("testCriteria: {}", list);
        list = sessionFactory.openSession().createQuery("select e from UserEntity e").getResultList();
        log.info("testCriteria: {}", list);
    }

}
