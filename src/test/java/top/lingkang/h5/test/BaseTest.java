package top.lingkang.h5.test;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;
import org.junit.runner.RunWith;
import org.noear.solon.annotation.Import;
import org.noear.solon.annotation.Inject;
import org.noear.solon.test.SolonJUnit4ClassRunner;
import top.lingkang.h5.test.dao.OrderDao;
import top.lingkang.h5.test.dao.UserDao;

/**
 * @author lingkang
 * created by 2023/10/31
 */
@Slf4j
@Import(profiles = "classpath:app.yml")
@RunWith(SolonJUnit4ClassRunner.class)
public class BaseTest {
    @Inject
    protected UserDao userDao;
    @Inject
    protected OrderDao orderDao;
    @Inject
    protected SessionFactory sessionFactory;
}
