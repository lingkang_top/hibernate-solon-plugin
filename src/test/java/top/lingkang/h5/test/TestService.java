package top.lingkang.h5.test;

import org.junit.Test;
import org.noear.solon.annotation.Inject;
import top.lingkang.h5.entity.UserEntity;
import top.lingkang.h5.test.service.UserService;
import top.lingkang.hibernate5.transaction.Transaction;

import java.util.List;

/**
 * @author lingkang
 * Created by 2023/10/7
 * 对于service的测试
 */
public class TestService extends BaseTest {
    @Inject
    private UserService userService;

    @Test
    public void testService() {
        List<UserEntity> all = userService.getAll();
        System.out.println(all);
        userService.updateSex(all.get(0).getId());
        System.out.println(userService.getAll());
    }

    @Test
    public void testMoreTran(){
        userService.testUpdate1();
        userService.testUpdate2();
        userService.testUpdate3();
    }

    @Transaction
    @Test
    public void testMoreTran2(){
        userService.testUpdate1();
        userService.testUpdate2();
        userService.testUpdate3();
    }
}
