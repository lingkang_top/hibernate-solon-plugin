package top.lingkang.h5.test;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.query.Query;
import org.junit.Test;
import top.lingkang.hibernate5.transaction.Transaction;

import java.util.List;

/**
 * @author lingkang
 * created by 2023/10/23
 */
@Slf4j
public class TestStatelessSession extends BaseTest {

    @Test
    public void testStatic() {
        StatelessSession session = sessionFactory.openStatelessSession();
        List list = session.createNativeQuery("show status like '%thread%'").list();
        log.info(list.toString());
    }

    @Test
    public void testStatic2() {
        StatelessSession session = sessionFactory.openStatelessSession();
        List list = session.createNativeQuery("select e.* from n_user e").list();
        log.info(list.toString());
    }

    @Transaction
    @Test
    public void testStatic3() {
        StatelessSession session = sessionFactory.openStatelessSession();
        List list = session.createNativeQuery("select e.* from n_user e").list();
        log.info(list.toString());
    }

}
