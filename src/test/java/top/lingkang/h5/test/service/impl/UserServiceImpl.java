package top.lingkang.h5.test.service.impl;

import org.hibernate.SessionFactory;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import top.lingkang.h5.test.dao.UserDao;
import top.lingkang.h5.entity.UserEntity;
import top.lingkang.h5.test.service.UserService;
import top.lingkang.hibernate5.transaction.Transaction;

import java.util.Date;
import java.util.List;

/**
 * @author lingkang
 * Created by 2023/10/7
 */
@Transaction
@Component
public class UserServiceImpl implements UserService {
    @Inject
    private UserDao userDao;
    @Inject
    private SessionFactory sessionFactory;

    @Override
    public void updateSex(String id) {
        UserEntity entity = userDao.findById(id);
        if ("男".equals(entity.getSex()))
            entity.setSex("女");
        else
            entity.setSex("男");
        sessionFactory.getCurrentSession().save(entity);
    }

    @Override
    public List<UserEntity> getAll() {
        return userDao.findAll();
    }

    @Transaction
    @Override
    public void testUpdate1() {
        UserEntity result = sessionFactory.getCurrentSession()
                .createQuery("select e from UserEntity e",UserEntity.class)
                .setMaxResults(1).getSingleResult();
        userDao.updateTime(result.getId(),new Date());
    }

    @Transaction
    @Override
    public void testUpdate2() {
        UserEntity result = sessionFactory.getCurrentSession()
                .createQuery("select e from UserEntity e",UserEntity.class)
                .setMaxResults(1).getSingleResult();
        userDao.updateTime(result.getId(),new Date());
    }

    @Transaction
    @Override
    public void testUpdate3() {
        UserEntity result = sessionFactory.getCurrentSession()
                .createQuery("select e from UserEntity e",UserEntity.class)
                .setMaxResults(1).getSingleResult();
        userDao.updateTime(result.getId(),new Date());
    }
}
