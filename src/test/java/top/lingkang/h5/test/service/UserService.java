package top.lingkang.h5.test.service;

import top.lingkang.h5.entity.UserEntity;

import java.util.List;

/**
 * @author lingkang
 * Created by 2023/10/7
 */
public interface UserService {
    void updateSex(String id);

    List<UserEntity> getAll();

    void testUpdate1();

    void testUpdate2();

    void testUpdate3();
}
