package top.lingkang.h5.test.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author lingkang
 * Created by 2023/9/28
 */
@Data
public class UserVO implements Serializable {
    private String id;
    private String username;
}
