package top.lingkang.h5.test;

import org.hibernate.SessionFactory;
import org.noear.solon.Solon;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.SolonMain;
import top.lingkang.h5.test.dao.UserDao;
import top.lingkang.h5.entity.UserEntity;
import top.lingkang.hibernate5.transaction.Transaction;

import java.util.List;
import java.util.UUID;

/**
 * @author lingkang
 * created by 2023/10/7
 * 对于web的测试
 */
@Controller
@SolonMain
public class TestWeb {
    public static void main(String[] args) {
        Solon.start(TestWeb.class, args);
    }

    @Inject
    private UserDao userDao;
    @Inject
    private SessionFactory sessionFactory;

    @Mapping("/")
    public Object hello() {
        return userDao.findAll();
    }

    @Transaction
    @Mapping("/1")
    public Object c1() {
        userDao.updatePassword(UUID.randomUUID().toString());
        return null;
    }

    @Mapping("/t1")
    public Object t1() {
        return userDao.findAll();
    }

    @Mapping("/t2")
    public Object t2() {
        return userDao.findAll();
    }

    @Mapping("/t3")
    public Object t3() {
        return userDao.findAll();
    }

    @Mapping("/t4")
    public Object t4() {
        return userDao.findAll();
    }

    @Mapping("/t5")
    public Object t5() {
        return userDao.findAll();
    }

    @Mapping("/t6")
    public Object t6() {
        return userDao.findAll();
    }

    @Transaction
    @Mapping("/2")
    public Object c2() {
        System.out.println(userDao.updatePasswordInt(UUID.randomUUID().toString()));
        return "Hello world!";
    }

    @Transaction
    @Mapping("/3")
    public Object c3() {
        System.out.println(userDao.updatePassword3(UUID.randomUUID().toString()));
        return "Hello world!";
    }

    @Transaction
    @Mapping("/4")
    public Object c4() {
        System.out.println(userDao.updatePassword4(UUID.randomUUID().toString()));
        System.out.println(userDao.updatePassword41(UUID.randomUUID().toString()));
        userDao.updatePassword42(UUID.randomUUID().toString());
        return "Hello world!";
    }

    // @Transaction
    @Mapping("/5")
    public Object c5() {
        List list = sessionFactory.getCurrentSession().createQuery(
                "select e from UserEntity e"
        ).list();
        System.out.println(list);
        return "Hello world!";
    }

    @Mapping("/e")
    public Object e() {
        UserEntity entity = sessionFactory.createEntityManager().find(UserEntity.class, "admin");
        return entity;
    }
}
