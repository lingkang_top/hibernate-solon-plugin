package top.lingkang.h5.test.dao;

import top.lingkang.h5.entity.OrderEntity;
import top.lingkang.hibernate5.dao.Dao;
import top.lingkang.hibernate5.dao.HibernateDao;
import top.lingkang.hibernate5.dao.Query;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author lingkang
 * created by 2023/9/28
 */
@Dao
public interface OrderDao extends HibernateDao<OrderEntity> {

    @Query("select e.price as price from OrderEntity e where e.id=?1")
    BigDecimal getPrice(String id);

    @Query("select e.price as price from OrderEntity e")
    List<BigDecimal> getPriceList();

    @Query("select e from OrderEntity e where e.id=?1")
    Object getObj(String id);

    @Query("select e from OrderEntity e")
    List<Object> getObjList();

    @Query("select e from OrderEntity e where id=?1")
    OrderEntity getOrder(String id);

    int errorTest();
}
