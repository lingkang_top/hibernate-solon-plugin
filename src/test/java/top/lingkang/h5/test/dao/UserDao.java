package top.lingkang.h5.test.dao;

import top.lingkang.h5.entity.UserEntity;
import top.lingkang.h5.test.vo.UserVO;
import top.lingkang.hibernate5.dao.Dao;
import top.lingkang.hibernate5.dao.HibernateDao;
import top.lingkang.hibernate5.dao.Modifying;
import top.lingkang.hibernate5.dao.Query;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author lingkang
 * created by 2023/9/28
 */
@Dao
public interface UserDao extends HibernateDao<UserEntity> {
    @Query(value = "select e from UserEntity e")
    List<UserEntity> getAll();

    @Query("select e.id as id,e.username from UserEntity e")
    List<UserVO> getAllVo();

    @Modifying
    @Query("update UserEntity set password=?1")
    void updatePassword(String password);

    @Modifying
    @Query("update UserEntity set password=?1")
    int updatePasswordInt(String password);

    @Modifying
    @Query("update UserEntity set password=?1")
    Integer updatePassword3(String password);

    @Modifying
    @Query("update UserEntity set password=?1")
    Long updatePassword4(String password);
    @Modifying
    @Query("update UserEntity set password=?1")
    long updatePassword41(String password);
    @Modifying
    @Query("update UserEntity set password=?1")
    void updatePassword42(String password);
    @Modifying
    @Query("update UserEntity set password=?1")
    short updatePassword43(String password);

    @Modifying
    @Query("delete from UserEntity where id=?1")
    int delete1(String id);

    @Query("select count(e) from UserEntity e")
    long oneCount();

    @Query("select e.id as id,e.username as username from UserEntity e where id=?1")
    Map oneMap(String id);

    @Query("select e from UserEntity e where id=?1")
    Map oneMapEntity(String id);

    @Query("select e.id as id,e.password as password from UserEntity e")
    List<UserEntity> selectEntity();

    @Query("select count(e.id)>1 from UserEntity e")
    boolean exists();

    @Modifying
    @Query("update UserEntity set updateTime=?2 where id=?1")
    int updateTime(String id, Date updateTime);
}
