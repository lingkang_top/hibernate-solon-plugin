package top.lingkang.h5.test;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.junit.Test;
import top.lingkang.h5.entity.OrderEntity;
import top.lingkang.h5.entity.UserEntity;
import top.lingkang.h5.test.vo.UserVO;
import top.lingkang.hibernate5.transaction.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author lingkang
 * created by 2023/9/28
 * 对于dao的测试
 */
@Slf4j
public class TestDao extends BaseTest {

    @Test
    public void read() {
        System.out.println(userDao.findAll());
        System.out.println(orderDao.findAll());
    }

    @Transaction
    @Test
    public void addAndDelete() {
        UserEntity entity = new UserEntity();
        entity.setId(System.currentTimeMillis() + "");
        entity.setSex("男");
        entity.setNickname("lingkang");
        userDao.saveOrUpdate(entity);
        // userDao.flush();
        System.out.println(entity);
        // System.out.println(userDao.findAll());
        userDao.deleteById(entity.getId());
    }

    // @Transaction
    @Test
    public void add() {
        Session session = sessionFactory.openSession();
        log.info("session {}",session);
        session.getTransaction().begin();
        // session.getTransaction().begin();
        UserEntity entity = new UserEntity();
        entity.setId(System.currentTimeMillis() + "");
        entity.setSex("男");
        entity.setNickname("lingkang");
        session.save(entity);
        session.flush();
        session.close();
        // session.getTransaction().commit();
        Session session1 = sessionFactory.openSession();
        log.info("session1 {}",session1);
        List select_e_from_userEntity_e = session1.createQuery("select e from UserEntity e").list();
        // session.getTransaction().commit();
        // userDao.saveOrUpdate(entity);
        session1.close();
    }

    @Transaction
    @Test
    public void add2() {
        UserEntity entity = new UserEntity();
        entity.setId(System.currentTimeMillis() + "");
        entity.setSex("男");
        entity.setNickname("lingkang");
        userDao.saveOrUpdate(entity);
        /*sessionFactory.openSession().createQuery(
                "INSERT INTO UserEntity VALUES(?,?,?)")
                .setParameter(1,System.currentTimeMillis()+"")
                .setParameter(2,"lingkang")
                .setParameter(3,"asdsadasdasdsadas").executeUpdate();*/
    }

    @Test
    public void getAll() {
        System.out.println(userDao.getAll());
    }

    @Test
    public void getAllVo() {
        List<UserVO> allVo = userDao.getAllVo();
        System.out.println(allVo);
    }


    @Transaction
    @Test
    public void updateTest() {
        userDao.updatePassword(UUID.randomUUID().toString());
    }

    @Transaction
    @Test
    public void updateTest2() {
        System.out.println(userDao.updatePasswordInt(UUID.randomUUID().toString()));
    }

    @Transaction
    @Test
    public void updateTest3() {
        System.out.println(userDao.updatePassword3(UUID.randomUUID().toString()));
    }

    @Transaction
    @Test
    public void updateTest4() {
        System.out.println(userDao.updatePassword4(UUID.randomUUID().toString()));
    }

    @Transaction
    @Test
    public void deleteTest1() {
        UserEntity entity = new UserEntity();
        entity.setId(System.currentTimeMillis() + "");
        entity.setSex("男");
        entity.setNickname("lingkang");
        userDao.saveOrUpdate(entity);
        userDao.getSession().flush();
        System.out.println(userDao.delete1(entity.getId()));
    }

    @Transaction
    @Test
    public void deleteTest2() {
        UserEntity entity = new UserEntity();
        entity.setId(System.currentTimeMillis() + "");
        entity.setSex("男");
        entity.setNickname("lingkang");
        userDao.saveOrUpdate(entity);
        System.out.println(userDao.getAll());
        userDao.delete(entity);
    }

    private String delId = "";

    @Transaction
    @Test
    public void deleteTest3() {
        UserEntity entity = new UserEntity();
        entity.setId(System.currentTimeMillis() + "");
        entity.setSex("男");
        entity.setNickname("lingkang");
        userDao.saveOrUpdate(entity);
        delId = entity.getId();
    }

    @Transaction
    @Test
    public void deleteTest4() {
        int deleteById = userDao.deleteById(delId);
        log.info("deleteById: {}", deleteById);
    }


    @Test
    public void testOne() {
        System.out.println(userDao.oneCount());
    }

    @Test
    public void testOneMap() {
        UserEntity entity = userDao.getAll().get(0);
        log.info("testOneMap:{}", userDao.oneMap(entity.getId()));
    }

    @Test
    public void testOneMapEntity() {
        UserEntity entity = userDao.getAll().get(0);
        log.info("testOneMapEntity: {}", userDao.oneMapEntity(entity.getId()));
    }

    @Test
    public void testOneBigDecimal() {
        OrderEntity order = orderDao.findAll().get(0);
        log.info("testOneBigDecimal: {}", orderDao.getPrice(order.getId()));
    }

    @Test
    public void testOneBigDecimalList() {
        log.info("testOneBigDecimalList: {}", orderDao.getPriceList());
    }

    @Test
    public void testOneObj() {
        OrderEntity order = orderDao.findAll().get(0);
        log.info("testOneObj: {}", orderDao.getObj(order.getId()));
    }

    @Test
    public void testOneObjList() {
        log.info("testOneObjList: {}", orderDao.getObjList());
    }

    @Test
    public void testOrder() {
        OrderEntity order = orderDao.findAll().get(0);
        log.info("testOrder: {}", orderDao.getOrder(order.getId()));
        log.info("testOrder: {}", orderDao.getOrder("test"));
    }

    @Test
    public void testError() {
        try {
            int i = orderDao.errorTest();
        } catch (Exception e) {
            log.warn("错误测试正确：{}", e.getMessage());
        }
    }

    @Test
    public void selectEntity() {
        List<UserEntity> list = userDao.selectEntity();
        log.info("selectEntity: {}", list);
    }

    @Test
    public void exists() {
        boolean exists = userDao.exists();
        log.info("exists: {}", exists);
    }

    @Test
    public void isExistsById() {
        List<UserEntity> all = userDao.getAll();
        boolean exists = userDao.isExistsById(all.get(0).getId());
        log.info("isExistsById: {}", exists);
        boolean exists2 = userDao.isExistsById(System.currentTimeMillis() + "");
        log.info("exists2: {}", exists2);
    }

    @Transaction
    @Test
    public void deleteAll() {
        List<UserEntity> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            UserEntity entity = new UserEntity();
            entity.setPassword(UUID.randomUUID().toString());
            entity.setUsername(i + "");
            entity.setId(UUID.randomUUID().toString());
            list.add(entity);
        }
        List<UserEntity> saveAll = userDao.saveAll(list);
        log.info("saveAll: {}", saveAll);

        userDao.deleteAll(saveAll);
    }

    @Transaction
    @Test
    public void deleteAllFlush() {
        List<UserEntity> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            UserEntity entity = new UserEntity();
            entity.setPassword(UUID.randomUUID().toString());
            entity.setUsername(i + "");
            entity.setId(UUID.randomUUID().toString());
            list.add(entity);
        }
        List<UserEntity> saveAll = userDao.saveAllAndFlush(list);
        log.info("saveAllAndFlush: {}", saveAll);

        userDao.deleteAll(saveAll);
    }

    @Test
    public void testCount() {
        long all = userDao.countAll();
        log.info("testCount: {}", all);
    }
}
