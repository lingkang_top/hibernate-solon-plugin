package top.lingkang.h5.test.demo;

/**
 * @author lingkang
 * created by 2023/9/28
 */
public class Demo01 {
    public static void main(String[] args) {
        System.out.println(int.class.getName());
        System.out.println(long.class.getName());
        System.out.println(Integer.class.getName());
        System.out.println(Long.class.getName());
    }
}
