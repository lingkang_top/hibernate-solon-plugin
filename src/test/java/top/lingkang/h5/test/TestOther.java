package top.lingkang.h5.test;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.junit.Test;

import java.util.List;

/**
 * @author lingkang
 * created by 2023/10/23
 */
@Slf4j
public class TestOther extends BaseTest {
    @Test
    public void testDb() {
        Session session = sessionFactory.getCurrentSession();
        List list = session.createNativeQuery("show status like '%thread%'").list();
        log.info(list.toString());
    }

    @Test
    public void testQueryName() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select e from UserEntity e");
        sessionFactory.addNamedQuery("get_all_user", query);
        List allUser = session.createNamedQuery("get_all_user").list();
        System.out.println(allUser);
    }

    @Test
    public void testInp() {
    }

}
