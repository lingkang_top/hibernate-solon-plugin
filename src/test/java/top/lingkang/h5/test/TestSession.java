package top.lingkang.h5.test;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.query.criteria.internal.OrderImpl;
import org.junit.Test;
import top.lingkang.h5.entity.UserEntity;
import top.lingkang.hibernate5.dao.TupleResult;
import top.lingkang.hibernate5.transaction.Transaction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author lingkang
 * created by 2023/9/27
 * 对于session的厕所
 */
@Slf4j
public class TestSession extends BaseTest {

    @Transaction
    @Test
    public void read() {
        Query query = sessionFactory.openSession().createQuery(
                "select e from UserEntity e"
        );
        List<UserEntity> resultList = query.getResultList();
        log.info("resultList: {}", resultList);

        Session session = sessionFactory.getCurrentSession();
        session.createQuery(
                "update UserEntity set password='asdasdasdas'"
        ).executeUpdate();
        // 提交事务
        // session.beginTransaction().commit();
        List list = session.createQuery(
                "select e from UserEntity e where id='admin'"
        ).getResultList();
        log.info("list={}", list);
//        if (1 == 1)
//            throw new RuntimeException("1");
    }

    @Test
    public void sessionTest() {
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        List list = session.createQuery(
                "select e from UserEntity e"
        ).list();
        log.info("sessionTest: {}", list);
        sessionTest2();
        session.getTransaction().commit();
    }

    @Test
    public void sessionTest2() {
        List list = sessionFactory.openSession().createQuery(
                "select e from UserEntity e"
        ).list();
        System.out.println(list);
    }

    @Transaction// 更新/删除操作必须使用事务，遵循hibernate
    @Test
    public void tr() {
        sessionFactory.getCurrentSession().createQuery("update UserEntity set updateTime=?1")
                .setParameter(1, new Date()).executeUpdate();
    }

    @Test
    public void testPage() {
        List list = sessionFactory.openSession().createQuery(
                "select e from UserEntity e"
        ).setMaxResults(10).setFirstResult(1).list();
        log.info("分页结果： {}", list);
    }

    @Test
    public void testPage2() {
        List list = sessionFactory.openSession().createQuery(
                        "select e.id as id,(select c.nickname from UserEntity c where c.id=e.id) as nickname" +
                                " from UserEntity e"
                ).setResultTransformer(new TupleResult(Map.class))
                .setMaxResults(10).setFirstResult(1)
                .list();
        log.info("分页结果： {}", list);
    }

    @Test
    public void testPage3() {
        List list = sessionFactory.openSession().createQuery(
                        "select e.id,e.nickname from UserEntity e"
                ).setResultTransformer(new TupleResult(Map.class))
                .setMaxResults(10).setFirstResult(1)
                .list();
        log.info("testPage3分页结果： {}", list);
    }

    @Test
    public void testCriteria() {
        Session currentSession = sessionFactory.openSession();
        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<UserEntity> criteria = builder.createQuery(UserEntity.class);
        Root<UserEntity> from = criteria.from(UserEntity.class);
        criteria.select(from);
        List<Predicate> predicates = new ArrayList<>();

        // 假设条件
        if ("id".equals("id"))
            predicates.add(builder.equal(from.get("id"), "admin123"));
        if ("id".equals("id"))
            predicates.add(builder.like(from.get("id"), "%a%"));
        criteria.where(predicates.toArray(new Predicate[0]));
        // 排序
        criteria.orderBy(
                new OrderImpl(from.get("id"), true),
                new OrderImpl(from.get("createTime"), false)
        );// true 为 asc，false为desc

        List<UserEntity> resultList = currentSession.createQuery(criteria).getResultList();
        log.info("testCriteria: {}", resultList);
    }

    @Transaction
    @Test
    public void testEM() {
        EntityManager entityManager = sessionFactory.createEntityManager();
        List resultList = entityManager.createQuery("select e from UserEntity e").getResultList();
        log.info("testEM: {}", resultList);
    }

    @Test
    public void test_() {
        List list = sessionFactory.openSession().createQuery(
                "select e from UserEntity e where id=?1"
        ).setParameter(1, "admin").list();
        log.info("test_: {}", list);
    }

    @Transaction
    @Test
    public void testNotTransaction() {
        List list = sessionFactory.getCurrentSession().createQuery(
                "select e from UserEntity e where id=?1"
        ).setParameter(1, "admin").list();
        log.info("testNotTransaction1: {}", list);

        list = sessionFactory.getCurrentSession().createQuery(
                "select e from UserEntity e where id=?1"
        ).setParameter(1, "admin").list();
        log.info("testNotTransaction2: {}", list);
    }

    @Transaction
    @Test
    public void testTransaction() {
        Session session = sessionFactory.openSession();
        List list = session.createQuery(
                "select e from UserEntity e where id=?1"
        ).setParameter(1, "admin").list();
        log.info("testNotTransaction1: {}", list);

        list = sessionFactory.getCurrentSession().createQuery(
                "select e from UserEntity e where id=?1"
        ).setParameter(1, "admin").list();
        log.info("testNotTransaction2: {}", list);
    }


}
