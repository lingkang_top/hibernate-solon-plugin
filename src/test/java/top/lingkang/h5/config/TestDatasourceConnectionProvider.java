package top.lingkang.h5.config;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.engine.jdbc.connections.internal.DatasourceConnectionProviderImpl;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author lingkang
 * Created by 2023/12/2
 */
@Slf4j
public class TestDatasourceConnectionProvider extends DatasourceConnectionProviderImpl {
    @Override
    public Connection getConnection() throws SQLException {
        Connection connection = super.getConnection();
        log.info("获取连接：{}", connection);
        return connection;
    }

    @Override
    public void closeConnection(Connection connection) throws SQLException {
        log.info("关闭连接：{}", connection);
        super.closeConnection(connection);
    }
}
