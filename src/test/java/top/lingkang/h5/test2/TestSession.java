package top.lingkang.h5.test2;

import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.cfg.Configuration;
import org.junit.Test;
import top.lingkang.h5.config.TestDatasourceConnectionProvider;
import top.lingkang.h5.entity.UserEntity;
import top.lingkang.hibernate5.config.HibernateConfiguration;

import java.util.List;
import java.util.Properties;

/**
 * @author lingkang
 * Created by 2023/12/2
 */
public class TestSession {
    public SessionFactory getSessionFactory(){
        HikariDataSource dataSource=new HikariDataSource();
        dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf8&autoReconnect=true&rewriteBatchedStatements=true");
        dataSource.setUsername("root");
        dataSource.setPassword("123456");
        dataSource.setPoolName("db1");
        dataSource.setMaximumPoolSize(10);
        Properties properties = new Properties();
        properties.put(AvailableSettings.DATASOURCE, dataSource);
        properties.put(AvailableSettings.CONNECTION_PROVIDER,new TestDatasourceConnectionProvider());
        properties.put(AvailableSettings.AUTOCOMMIT,true);

        properties.put(AvailableSettings.HBM2DDL_AUTO, "validate");
        properties.put(AvailableSettings.CURRENT_SESSION_CONTEXT_CLASS, "thread");

        properties.put(AvailableSettings.SHOW_SQL, "true");
        Configuration configuration=new Configuration();
        // 或者一个个加
        configuration.addAnnotatedClass(UserEntity.class);
        configuration.setProperties(properties);
        // configuration.setInterceptor(new MyInterceptor());
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        return sessionFactory;
    }

    @Test
    public void add(){
        SessionFactory sessionFactory = getSessionFactory();
        System.out.println("--------------------------------------------------");
        Session currentSession = sessionFactory.getCurrentSession();
        System.out.println(currentSession);
        Session session = sessionFactory.openSession();
        System.out.println(session);
        session.getTransaction().begin();
        // Transaction transaction = session.getTransaction();
        // transaction.begin();
        UserEntity entity = new UserEntity();
        entity.setId(System.currentTimeMillis() + "");
        entity.setSex("男");
        entity.setNickname("lingkang");
        session.persist(entity);
        List list = session.createQuery("select e from UserEntity e").list();
        session.getTransaction().commit();
        // transaction.commit();
    }
}
