package top.lingkang.h5.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author lingkang
 * created by 2023/9/28
 * <br/>
 * <pre>
 *     CREATE TABLE `n_order`
 * (
 *     `id`          varchar(50) NOT NULL,
 *     `user_id`     varchar(50)   DEFAULT NULL,
 *     `name`        varchar(50)   DEFAULT NULL,
 *     `description` varchar(255)  DEFAULT NULL,
 *     `price`       decimal(6, 2) DEFAULT NULL,
 *     `status`      varchar(1)    DEFAULT NULL,
 *     `create_time` datetime      DEFAULT NULL,
 *     `update_time` datetime      DEFAULT NULL,
 *     PRIMARY KEY (`id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 * </pre>
 */
@Data
@Entity
@Table(name = "n_order")
public class OrderEntity {
    @Id
    private String id;
    @Column(name = "user_id")
    private String userId;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private BigDecimal price;
    @Column
    private String status;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "update_time")
    private Date updateTime;

    @PrePersist
    public void prePersist() {
        if (createTime == null)
            createTime = new Date();
        if (updateTime == null)
            updateTime = createTime;
        else
            updateTime = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        updateTime = new Date();
    }
}
