CREATE TABLE `n_user`
(
    `id`          varchar(50) NOT NULL,
    `username`    varchar(50) DEFAULT NULL,
    `password`    varchar(64) DEFAULT NULL,
    `nickname`    varchar(50) DEFAULT NULL,
    `sex`         varchar(2)  DEFAULT NULL,
    `create_time` datetime    DEFAULT NULL,
    `update_time` datetime    DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `n_order`
(
    `id`          varchar(50) NOT NULL,
    `user_id`     varchar(50)   DEFAULT NULL,
    `name`        varchar(50)   DEFAULT NULL,
    `description` varchar(255)  DEFAULT NULL,
    `price`       decimal(6, 2) DEFAULT NULL,
    `status`      varchar(1)    DEFAULT NULL,
    `create_time` datetime      DEFAULT NULL,
    `update_time` datetime      DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `n_user` (`id`, `username`, `password`, `nickname`, `sex`, `create_time`, `update_time`) VALUES ('1696729980802', NULL, NULL, 'lingkang', '男', '2023-10-08 09:53:01', '2023-10-08 09:53:01');
INSERT INTO `n_user` (`id`, `username`, `password`, `nickname`, `sex`, `create_time`, `update_time`) VALUES ('admin', 'admin', 'asdasdasdas', 'admin', '男', '2023-09-05 18:43:16', '2023-10-08 09:10:56');


INSERT INTO `n_order` (`id`, `user_id`, `name`, `description`, `price`, `status`, `create_time`, `update_time`) VALUES ('506f9c24-6be7-4c23-b25f-57784ba48f18', 'admin', '123', NULL, 66.66, '1', '2023-08-22 16:05:50', '2023-08-22 16:05:50');
INSERT INTO `n_order` (`id`, `user_id`, `name`, `description`, `price`, `status`, `create_time`, `update_time`) VALUES ('79573096-6d62-46ee-9c82-d72c6401e782', 'admin', '随机', NULL, 66.66, '1', '2023-09-07 14:33:35', '2023-09-07 14:33:35');
INSERT INTO `n_order` (`id`, `user_id`, `name`, `description`, `price`, `status`, `create_time`, `update_time`) VALUES ('f4cf53bf-a321-49d3-8c7d-d41eb1bc6296', 'admin', '随机', NULL, 66.66, '1', '2023-08-22 16:05:28', '2023-08-22 16:05:28');