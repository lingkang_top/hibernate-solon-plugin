package top.lingkang.hibernate5.transaction;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author lingkang<br />
 * created by 2023/9/28 <br/> <br/>
 * 事务注解，作用于类、方法上<br/>
 * 事务传播机制：如果当前存在事务，则加入该事务，否则新建一个事务。不实现其他机制，保持hibernate整体事务<br/>
 * <br/>
 * <p>
 * 注意：针对 Controller、Service、Dao 等所有基于MethodWrap运行的目标，才有效。<br/>
 * 在hibernate高版本中（5.x以上）已经弃用当前对当前session进行设置事务隔离级别，<br/>
 * 推荐设置全局事务级别：{@link org.hibernate.cfg.AvailableSettings } 中的 ISOLATION
 * <br/>
 * <pre>
 * // 例如 <br/>
 * Properties properties = new Properties();<br/>
 * properties.put(AvailableSettings.ISOLATION, 1);
 * </pre>
 * @since 1.0
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Transaction {
}
