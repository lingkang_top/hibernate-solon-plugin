package top.lingkang.hibernate5.dao;

import java.lang.reflect.Method;

/**
 * @author lingkang
 * created by 2023/10/8
 */
public interface QueryHandler {
    Object doQuery(Method method, Object[] args) throws Throwable;
}
