package top.lingkang.hibernate5.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.noear.solon.Solon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.lingkang.hibernate5.util.CheckUtils;

import javax.persistence.Id;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

/**
 * @author lingkang
 * created by 2023/9/28
 * CURD 封装，其他复杂sql请使用 @Query
 * @since 1.0.0
 */
public class HibernateDaoImpl implements HibernateDao {
    private static final Logger log = LoggerFactory.getLogger(HibernateDaoImpl.class);

    private Class<?> entityClass;
    private String idColumnName = null;
    private SessionFactory sessionFactory;

    public HibernateDaoImpl(Class<?> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public List findAll() {
        return getSession().createQuery(
                "select e from " + entityClass.getSimpleName() + " e"
        ).list();
    }

    @Override
    public Object findById(Serializable id) {
        CheckUtils.checkNotNull(id, "Id不能为空");
        return getSession().find(entityClass, id);
    }

    @Override
    public Object saveOrUpdate(Object entity) {
        CheckUtils.checkNotNull(entity, "保存对象不能为空");
        getSession(true).persist(entity);// 保存或更新
        return entity;
    }

    @Override
    public List saveAll(List entities) {
        CheckUtils.checkListNotNull(entities, "保存列表不能为空");
        Session session = getSession(true);
        for (Object e : entities)
            session.persist(e);// 保存或更新
        return entities;
    }

    @Override
    public List saveAllAndFlush(List entities) {
        List list = saveAll(entities);
        flush();
        return list;
    }

    @Override
    public void delete(Object entity) {
        CheckUtils.checkNotNull(entity, "删除对象不能为空");
        Session session = getSession(true);
        session.delete(entity);
        session.flush();
    }

    @Override
    public int deleteById(Serializable id) {
        CheckUtils.checkNotNull(id, "Id不能为空");
        return getSession(true).createQuery("delete from " + entityClass.getSimpleName() + " where " + getIdColumnName() + "=?1")
                .setParameter(1, id).executeUpdate();
    }

    @Override
    public void deleteAll(List entities) {
        CheckUtils.checkListNotNull(entities, "删除列表不能为空");
        Session session = getSession(true);
        for (Object e : entities)
            session.delete(e);// 保存或更新
    }

    @Override
    public boolean isExistsById(Serializable id) {
        List<Boolean> list = getSession().createQuery(
                        "select count(e)>0 from " + entityClass.getSimpleName() + " e where " + getIdColumnName() + "=?1",
                        Boolean.class
                )
                .setParameter(1, id).list();
        return list.get(0);
    }

    @Override
    public long countAll() {
        Long count = getSession().createQuery(
                "select count(e) from " + entityClass.getSimpleName() + " e",
                Long.class
        ).getSingleResult();
        return count;
    }

    /**
     * 通常是在最后提交阶段执行，中途执行会花费相对昂贵的java代价，线程不安全，多次执行可能会报空指针问题。
     */
    @Override
    public void flush() {
        getSession().flush();
    }

    public Session getSession() {
        return getSession(false);
    }

    private Session getSession(boolean checkTransaction) {
        if (sessionFactory == null) {
            sessionFactory = Solon.context().getBean(SessionFactory.class);
        }
        Session session = sessionFactory.openSession();
        if (checkTransaction && !session.getTransaction().isActive())
            throw new IllegalStateException("事务还未开启：Transaction is not open, update/delete need transaction");

        return session;
    }

    private String getIdColumnName() {
        if (idColumnName != null)
            return idColumnName;
        Field[] fields = entityClass.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Id.class)) {
                idColumnName = field.getName();
                return idColumnName;
            }
        }
        throw new IllegalArgumentException("当前实体类不存在@Id列：" + entityClass.getName());
    }
}
