package top.lingkang.hibernate5.dao.impl;

import top.lingkang.hibernate5.dao.HibernateDaoImpl;
import top.lingkang.hibernate5.dao.QueryHandler;

import java.lang.reflect.Method;

/**
 * @author lingkang
 * created by 2023/10/8
 */
public class DaoQueryHandler implements QueryHandler {
    private HibernateDaoImpl daoImpl;
    private Class<?> entityClass;

    public DaoQueryHandler(Class<?> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public Object doQuery(Method method, Object[] args) throws Throwable {
        if (daoImpl == null)
            daoImpl = new HibernateDaoImpl(entityClass);

        try {
            Method impl = daoImpl.getClass().getDeclaredMethod(method.getName(), method.getParameterTypes());
            return impl.invoke(daoImpl, args);
        } catch (Exception e) {
            throw e;
        }
    }
}
