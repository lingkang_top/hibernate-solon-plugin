package top.lingkang.hibernate5.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import top.lingkang.hibernate5.dao.OneResultTransformer;
import top.lingkang.hibernate5.dao.Query;
import top.lingkang.hibernate5.dao.QueryHandler;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lingkang
 * created by 2023/10/8
 */
public class OneQueryHandler implements QueryHandler {
    private SessionFactory sessionFactory;
    private Class<?> returnType;
    private Query query;
    private boolean isNative;

    public OneQueryHandler(SessionFactory sessionFactory, Class<?> returnType, Query query, boolean isNative) {
        this.sessionFactory = sessionFactory;
        this.returnType = returnType;
        this.query = query;
        this.isNative = isNative;
    }

    @SuppressWarnings("deprecation")
    @Override
    public Object doQuery(Method method, Object[] args) throws Throwable {
        List list = getQuery(query, args).setResultTransformer(new OneResultTransformer(returnType)).list();
        if (list.isEmpty())
            return returnEmpty();
        if (list.size() > 1)
            throw new IllegalArgumentException("查询结果有多条数据，接收类型错误：List 不能转化为 " + returnType.getName());
        return list.get(0);
    }

    private Object returnEmpty() {
        if (returnType == Map.class)
            return new HashMap<>();
        if (returnType == int.class || returnType == long.class || returnType == Integer.class || returnType == Long.class
                || returnType == short.class || returnType == Short.class)
            return 0;
        return null;
    }

    private org.hibernate.query.Query getQuery(Query query, Object[] args) {
        Session session = sessionFactory.openSession();
        org.hibernate.query.Query exeQuery = null;
        if (isNative) {
            exeQuery = session.createNativeQuery(query.value());
        } else
            exeQuery = session.createQuery(query.value());
        if (args != null)
            for (int i = 0; i < args.length; i++)
                exeQuery.setParameter(i + 1, args[i]);
        return exeQuery;
    }
}
