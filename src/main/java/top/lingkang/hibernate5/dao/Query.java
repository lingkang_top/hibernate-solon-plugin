package top.lingkang.hibernate5.dao;

import java.lang.annotation.*;

/**
 * Dao接口的查询注解
 * 若涉及修改/删除，应该添加上 {@link Modifying}
 * 保持JPA习惯
 *
 * @author lingkang
 * created by 2023/9/28
 * @since 1.0.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Documented
public @interface Query {
    /**
     * sql 语句
     * 若涉及修改/删除，应该在接口上加上 {@link Modifying}
     */
    String value() default "";

    /**
     * 是否执行原生查询sql
     */
    boolean nativeQuery() default false;
}
