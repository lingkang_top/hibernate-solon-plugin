package top.lingkang.hibernate5.dao;

import java.lang.annotation.*;

/**
 * 若 {@link Query} 的操作涉及修改/删除，应该添加此注解
 * 保留了JPA习惯
 * <br>
 * <pre>
 * @Modifying <br>
 * @Query("update UserEntity set password=?1") <br>
 * void updatePassword(String password); <br>
 * </pre>
 *
 * @author lingkang
 * created by 2023/9/28
 * @since 1.0.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Modifying {
}
