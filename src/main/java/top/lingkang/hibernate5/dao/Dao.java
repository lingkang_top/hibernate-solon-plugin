package top.lingkang.hibernate5.dao;

import java.lang.annotation.*;

/**
 * @author lingkang
 * created by 2023/9/28
 * 作用于 继承了 {@link  top.lingkang.hibernate5.dao.HibernateDao} 接口的接口上
 * 用于CURD
 * @since 1.0.0
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited// 允许继承
public @interface Dao {
}
