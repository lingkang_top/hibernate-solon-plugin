package top.lingkang.hibernate5.dao;

import org.hibernate.transform.ResultTransformer;

import javax.persistence.Table;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lingkang
 * created by 2023/10/7
 */
public class OneResultTransformer implements ResultTransformer {
    private Class<?> returnType;

    public OneResultTransformer(Class<?> returnType) {
        this.returnType = returnType;
    }

    @Override
    public Object transformTuple(Object[] tuple, String[] aliases) {
        if (returnType == Map.class) {
            Map<String, Object> map = new HashMap<>();
            if (tuple.length == 1 && tuple[0].getClass().isAnnotationPresent(Table.class)) {
                copy(tuple[0], map);
            } else {
                for (int i = 0; i < aliases.length; i++) {
                    map.put(aliases[i], tuple[i]);
                }
            }
            return map;
        }
        if (tuple.length > 1)
            throw new IllegalArgumentException("返回结果有多个列，接收类型：" + returnType.getName());
        return tuple[0];
    }

    @Override
    public List transformList(List collection) {
        return collection;
    }

    protected void copy(Object source, Map tag) {
        for (Field field : source.getClass().getDeclaredFields()) {
            Object o = null;
            try {
                field.setAccessible(true);
                o = field.get(source);
                tag.put(field.getName(), o);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
