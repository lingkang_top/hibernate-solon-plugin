package top.lingkang.hibernate5.dao;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

/**
 * @author lingkang
 * created by 2023/9/28
 * CURD 封装，其他复杂sql请使用 @Query
 * @since 1.0.0
 */
public interface HibernateDao<E> {
    /**
     * 查询所有
     */
    List<E> findAll();

    /**
     * 根据Id查找，查找单个数据
     */
    E findById(Serializable id);

    /**
     * 保存或更新
     */
    E saveOrUpdate(E entity);

    /**
     * 批量保存，此时还滞留于缓存中
     */
    List<E> saveAll(List<E> entities);

    /**
     * 批量保存，将缓冲数据写入数据库
     */
    List<E> saveAllAndFlush(List<E> entities);

    /**
     * 删除实体，根据实体
     */
    void delete(E entity);

    /**
     * 删除数据根据ID
     */
    int deleteById(Serializable id);

    /**
     * 批量删除
     */
    void deleteAll(List<E> entities);

    /**
     * 是否存在对象，根据id查询
     */
    boolean isExistsById(Serializable id);

    /**
     * 统计表数据个数
     */
    long countAll();

    /**
     * 刷新持久性上下文，将操作持久化数据执行到库中
     * 原因：`hibernate`在每次执行完`update`/`delete`之后，会把数据先存放在缓存中，不会立即更新到数据库
     */
    void flush();

    Session getSession();
}
