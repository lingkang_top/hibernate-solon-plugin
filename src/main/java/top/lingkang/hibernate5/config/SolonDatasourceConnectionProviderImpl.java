package top.lingkang.hibernate5.config;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.engine.jdbc.connections.internal.DatasourceConnectionProviderImpl;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

/**
 * @author lingkang
 * created by 2023/12/1
 */
@Slf4j
class SolonDatasourceConnectionProviderImpl extends DatasourceConnectionProviderImpl {
    public SolonDatasourceConnectionProviderImpl() {
        log.info("init TransactionDatasourceConnectionProviderImpl");
    }

    private DataSource dataSource;

    @Override
    public void configure(Map configValues) {
        final Object dataSource = configValues.get(AvailableSettings.DATASOURCE);
        this.dataSource = (DataSource) dataSource;
    }

    @Override
    public Connection getConnection() throws SQLException {
        Connection connection = dataSource.getConnection();
        log.debug("获取jdbc连接：{}", connection);
        return connection;
    }

    @Override
    public void closeConnection(Connection connection) throws SQLException {
        log.debug("关闭jdbc连接：{}", connection);
        connection.close();
    }
}
