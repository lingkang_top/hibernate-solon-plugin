package top.lingkang.hibernate5.util;

import java.util.Collection;
import java.util.List;

/**
 * @author lingkang
 * created by 2023/9/28
 * @since 1.0.0
 */
public class CheckUtils {
    public static void checkNotNull(Object obj, String msg) {
        if (obj == null)
            throw new IllegalArgumentException(msg);
    }

    public static void checkListNotNull(List obj, String msg) {
        if (obj == null || obj.size() == 0)
            throw new IllegalArgumentException(msg);
    }

    public static void checkNotEmpty(Collection<?> coll, String msg) {
        if (coll == null || coll.isEmpty())
            throw new IllegalArgumentException(msg);
    }

    public static void checkNotEmpty(String str, String msg) {
        if (str == null || str.length() == 0)
            throw new IllegalArgumentException(msg);
    }

    public static void isTrueAssert(boolean bool, String msg) {
        if (!bool)
            throw new IllegalArgumentException(msg);
    }

    public static void isFalseAssert(boolean bool, String msg) {
        if (bool)
            throw new IllegalArgumentException(msg);
    }
}
