package top.lingkang.hibernate5.plugin;

import lombok.extern.slf4j.Slf4j;
import org.noear.solon.core.AppContext;
import org.noear.solon.core.Plugin;
import top.lingkang.hibernate5.dao.Dao;
import top.lingkang.hibernate5.dao.DaoBeanBuilder;
import top.lingkang.hibernate5.transaction.HibernateTranInterceptor;
import top.lingkang.hibernate5.transaction.Transaction;

/**
 * solon的入口插件
 *
 * @author lingkang
 * created by 2023/9/27
 * @since 1.0.0
 */
@Slf4j
public class HibernatePluginImpl implements Plugin {
    @Override
    public void start(AppContext context) throws Throwable {
        context.beanBuilderAdd(Dao.class, new DaoBeanBuilder());
        context.beanInterceptorAdd(Transaction.class, new HibernateTranInterceptor(), 1);
        log.info("加载 hibernate-solon-plugin 插件完成");
    }
}
